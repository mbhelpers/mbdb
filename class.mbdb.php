<?php
	
########################################################################
# (c) 2015 Mirko Brunner
# mirko.brunner@googlemail.com
# 
# Have fun.
########################################################################

class mbdb
{
	private $port = "";
	private $host = "127.0.0.1";
	private $user = "M";
	private $pass = "";
	private $db__ = "";

	public $s;
	public $result;
	public $stmt;
	public $nrows;
	
	public function __construct()
	{
		$ms = new mysqli($this->host, $this->user, $this->pass, $this->db__);
		
		if($ms->connect_errno)
		{
			echo "Failed to connect to MySQL: (" . $ms->connect_errno . ") " . $ms->connect_error.'<br />';
			return NULL;
		}
		else
		{
			$this->s = $ms;
			$ms = NULL;
		}		
	}
	
	
	public function query_assoc($sql)
	{
		$res = $this->s->query($sql);
		
		while($r = $res->fetch_array())
    	{
    		$arr[] = $r;
    	}
		
		$res->close();
        return $arr;
	}
	
	public function query_db($sql)
	{
		$res = $this->s->query($sql);
		$this->result = $res;
		$this->nrows = $this->result->num_rows;
		return $res;
	}
	
	public function fast_query($sql)
	{
		trigger_error('Deprecated use query_db instat', E_USER_NOTICE);
		return $this->query_db($sql);
	}
	
	public function unsave_query($sql)
	{
		trigger_error('Deprecated use query_db instat', E_USER_NOTICE);
		return $this->query_db($sql);
	}
	
	public function close_result()
	{
		$this->result->close();
	}
	
	
	
	
	
	
	/* PREPARE STATEMENTS */
	public function prepare($statement)
	{
		$st = $this->s->prepare($statement);
		
		if(!$st)
		{
			 echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error."<br />";
			 return false;
		}
		else
		{
			$this->stmt = $st;
		}
	}
		
	public function execute()
	{
		$b = $this->stmt->execute();
		$this->stmt->close();
		return $b;
	}
	
	
	
	
	
	
	
	
	/* helper */
	public function show_in_table($arr,$styles="",$class="")
	{
		$count = count($arr);
		$output = "";

		if($count>0)
		{
			reset($arr);
			$num = count(current($arr));
			
			if(isset($styles))
			{
				$output.="<table class=\"".$class."\" align=\"".$styles['align']."\" border=\"".$styles['border']."\" cellpadding=\"".$styles['cellpadding']."\" cellspacing=\"".$styles['cellspacing']."\" width=\"".$styles['width']."\">\n";
			}
			else
			{
				$output.= '<table cellpadding="0" cellspacing="0">'."\n";
			}
			
			$output.="<tr>\n";

			foreach(current($arr) as $key => $value)
			{
				$output.="<th>";
				$output.=$key."&nbsp;";
				$output.="</th>\n";
        	}

			$output.="</tr>\n";

			while ($curr_row = current($arr))
			{
				$output.="<tr>\n";
				$col = 1;

				while (false !== ($curr_field = current($curr_row)))
				{
					$output.="<td>";
					$output.=$curr_field."&nbsp;";
					$output.="</td>\n";
					next($curr_row);
					$col++;
				}
				
				while($col <= $num)
				{
					$output.="<td>&nbsp;</td>\n";
					$col++;
				}
				
				$output.="</tr>\n";
				next($arr);
			}
			
			$output.="</table>\n";
		}

		return $output;
	}
}


	
?>