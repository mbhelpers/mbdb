<?php

/*
http://php.net/manual/de/mysqli.quickstart.stored-procedures.php
*/


class mbdb
{
	private $port = "";
	private $host = "127.0.0.1";
	private $user = "M";
	private $pass = "";
	private $db__ = "";

	public $s;
	public $result;
	public $stmt;
	public $nrows;
	
	public function __construct()
	{
		$ms = new mysqli($this->host, $this->user, $this->pass, $this->db__);
		
		if($ms->connect_errno)
		{
			echo "Failed to connect to MySQL: (" . $ms->connect_errno . ") " . $ms->connect_error.'<br />';
			return NULL;
		}
		else
		{
			$this->s = $ms;
			$ms = NULL;
		}		
	}
	
	
	public function query_assoc($sql)
	{
		$res = $this->s->query($sql);
		
		while($r = $res->fetch_array())
    	{
    		$arr[] = $r;
    	}
		
		$res->close();
        return $arr;
	}
	
	public function query_db($sql)
	{
		$res = $this->s->query($sql);
		$this->result = $res;
		$this->nrows = $this->result->num_rows;
		return $res;
	}
	
	public function fast_query($sql)
	{
		trigger_error('Deprecated use query_db instat', E_USER_NOTICE);
		return $this->query_db($sql);
	}
	
	public function unsave_query($sql)
	{
		trigger_error('Deprecated use query_db instat', E_USER_NOTICE);
		return $this->query_db($sql);
	}
	
	public function close_result()
	{
		$this->result->close();
	}
	
	
	
	
	
	
	/* PREPARE STATEMENTS */
	public function prepare($statement)
	{
		$st = $this->s->prepare($statement);
		
		if(!$st)
		{
			 echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error."<br />";
			 return false;
		}
		else
		{
			$this->stmt = $st;
		}
	}
		
	public function execute()
	{
		$b = $this->stmt->execute();
		$this->stmt->close();
		return $b;
	}
	
	
	
	
	
	
	
	
	/* helper */
	public function show_in_table($arr,$styles="",$class="")
	{
		$count = count($arr);
		$output = "";

		if($count>0)
		{
			reset($arr);
			$num = count(current($arr));
			
			if(isset($styles))
			{
				$output.="<table class=\"".$class."\" align=\"".$styles['align']."\" border=\"".$styles['border']."\" cellpadding=\"".$styles['cellpadding']."\" cellspacing=\"".$styles['cellspacing']."\" width=\"".$styles['width']."\">\n";
			}
			else
			{
				$output.= '<table cellpadding="0" cellspacing="0">'."\n";
			}
			
			$output.="<tr>\n";

			foreach(current($arr) as $key => $value)
			{
				$output.="<th>";
				$output.=$key."&nbsp;";
				$output.="</th>\n";
        	}

			$output.="</tr>\n";

			while ($curr_row = current($arr))
			{
				$output.="<tr>\n";
				$col = 1;

				while (false !== ($curr_field = current($curr_row)))
				{
					$output.="<td>";
					$output.=$curr_field."&nbsp;";
					$output.="</td>\n";
					next($curr_row);
					$col++;
				}
				
				while($col <= $num)
				{
					$output.="<td>&nbsp;</td>\n";
					$col++;
				}
				
				$output.="</tr>\n";
				next($arr);
			}
			
			$output.="</table>\n";
		}

		return $output;
	}
}



class serv_db{
	private $port = "";

	private $host = "127.0.0.1";

	private $user = "";
	private $pass = "";
	private $db__ = "";

	public $linkid = 0;
	public $result = 0;
	public $nrows = 0;
	public $nfield = 0;
	public $lastid = 0;
	public $debug = true;

// Konstruktor ------------------------
	function serv_db(){
		if($this->connect_db()){
			$this->select_db();
			return true;
		}else{
			return false;
		}
	}

// Destruktor -------------------------
	function _destruct(){
		$this->close_db();
	}

// SET-FUNKTIONEN ---------------------
	function set_db($dbname){
		if($dbname==$thisdb__) return false;

		$this->close_db();
		$temp = $this->db__;
		$this->db__ = $dbname;

		if($this->connect_db()){
			$this->select_db();
			return true;
		}else{
			$this->db__ = $temp;
			return false;
		}
	}

// Haupthilfsfunktionen ---------------
	function close_db(){
		try{
			if(@mysql_close($this->linkid)){
				$this->linkid = 0;
				return true;
			}else{
				throw new Exception(mysql_errno());
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error());
			return false;
		}
	}



// Funktionen -------------------------------

// connect-----------------------
	function connect_db(){
		try{
			if($lk = @mysql_connect($this->host.":".$this->port,$this->user,$this->pass,false)){
				$this->linkid = $lk;
				return true;
			}else{
				throw new Exception(mysql_errno());
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error());
			return false;
		}
	}

// select------------------------
	private function select_db(){
		try{
			if(@mysql_select_db($this->db__,$this->linkid)){
				return true;
			}else{
				throw new Exception(mysql_errno());
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error());
			return false;
		}
	}

// query -----------------------
//			if_error = wenn fehlerausgabe erwuenscht (Oeberlagert $this->debug)
	public function query_db($statement,$if_error=false){

		//echo $statement."<br />";
        /*
		if($if_error){
			if(!is_string($statement)){
				echo $this->err(999999,"Abfrage-Syntax ist kein String!");
				return 0;
			}else if((strpos("SELECT",$statement)==-1)){
				echo strpos("SELECT",$statement);
				echo $this->err(999999,"Nur Abfragen sind erlaubt!");
				return 0;
			}
		}
        */
		if($this->debug==true) $if_error=true;

		try{
			if($res = @mysql_query($statement,$this->linkid)){
				$this->nrows = @mysql_num_rows($res);
				$this->nfield = @mysql_num_fields($res);
				$this->result = $res;
				return $this->result;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			if($if_error){
				echo $this->err($e->getMessage(),mysql_error($this->linkid),$statement);
				$this->nrows = 0;
				$this->nfield = 0;
				$this->result = 0;
				return 0;
			}else{
				return 0;
			}
		}
	}

    public function query_assoc($statement)
    {
        $this->query_db($statement);

    	while($r = mysql_fetch_assoc($this->result))
    	{
    		$arr[] = $r;
    	}

        return $arr;
    }

// insert ----------------------
//			if_error = ewnn fehlerausgabe erw�nscht	(�berlagert $this->debug)
	public function insert_db($statement,$if_error=false){
		if($if_error){
			if(!is_string($statement)){
				echo $this->err(999999,"Einf&uuml;gen-Syntax ist kein String!");
				return 0;
			}else if((strpos("INSERT",$statement)==-1)){
				echo strpos("INSERT",$statement);
				echo $this->err(999999,"Nur Einf&uuml;gen ist erlaubt!");
				return 0;
			}
		}

		if($this->debug==true) $if_error=true;

		try{
			if($res = @mysql_query($statement,$this->linkid)){
				$this->result = $res;
				return $this->result;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			if($if_error){
				echo $this->err($e->getMessage(),mysql_error($this->linkid),$statement);
				$this->result = 0;
				$this->lastid = 0;
				return 0;
			}else{
				return 0;
			}
		}
	}

	public function fast_query($statement,$if_error=false){
		if($this->debug==true) $if_error=true;
		try{
			if($res = @mysql_query($statement,$this->linkid)){
				$this->result = $res;
				return $this->result;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			if($if_error){
				echo $this->err($e->getMessage(),mysql_error($this->linkid),$statement);
				$this->result = 0;
				return 0;
			}else{
				return 0;
			}
		}
	}

	public function unsave_query($statement)
	{
		$this->result = mysql_query($statement, $this->linkid);
		return $this->result;
	}

	public function get_last_id(){
		try{
			if($iid = @mysql_insert_id($this->linkid)){
				$this->lastid = $iid;
				return $this->lastid;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			$this->lastid = 0;
			return 0;
		}
	}

	function count_result(){
		try{
			if($row = @mysql_fetch_object($this->result)){
				$this->nrows = $row;
				return $this->nrows;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			$this->nrows = 0;
			return 0;
		}
	}

	function count_rows(){
		try{
			if($row = @mysql_num_rows($this->result)){
				$this->nrows = $row;
				return $this->nrows;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			$this->nrows = 0;
			return 0;
		}
	}

	function count_fields(){
		try{
			if($row = @mysql_num_fields($this->result)){
				$this->nfield = $row;
				return $this->nfield;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			$this->nfield = 0;
			return 0;
		}
	}

	function move($offset){
		if(($offset<0) || !is_int($offset)) return false;

		try{
			if($bol = @mysql_data_seek($this->result,$offset)){
				$this->lastid = $this->get_last_id();
				return $bol;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			return false;
		}
	}

	function step(){
		try{
			if($bol = @mysql_data_seek($this->result,$this->get_last_id())){
				$this->lastid = $this->get_last_id();
				return $bol;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			return false;
		}

	}

	function save_fetch_array(){
		try{
			if($res = @mysql_fetch_array($this->result,MYSQL_ASSOC)){
				$this->lastid = $this->get_last_id();
				$this->nrows = $this->count_rows();
				$this->nfield = $this->count_fields();
				return $res;
			}else{
				throw new Exception(mysql_errno($this->linkid));
			}
		}catch(Exception $e){
			echo $this->err($e->getMessage(),mysql_error($this->linkid),"");
			return false;
		}
	}

    function i_know_it_is_a_unsave_fetch()
    {
        return mysql_fetch_array($this->linkid);
    }

    function unsave_fetch_array($ASSOC = true)
    {
        return mysql_fetch_array($this->linkid, ((!$ASSOC) ? false : $ASSOC));
    }

	private function err($e,$sql_text,$statement=""){
		//in diese Funktion noch logfile m�glichkeit hinnein f�r debug

		if(!$e) return;

		$text;

		switch($e){
			case 999999:
				$text = $sql_text;
				break;

			case 1045:
				if(!$this->debug){
					$text = "Datenbankeinwahl verweigert!<br>";
					$text.=	"Sie haben eventuell keine Berrechtigung f&uuml; diese Verbindung!<br><br>";
				}else{
					$text = "Datenbankeinwahl verweigert!<br>";
					$text.= "SQL-Fehlernummer:&nbsp;".$e."<br>";
					$text.= "HOST:&nbsp;".$this->host."<br>";
					$text.= "PORT:&nbsp;".$this->port."<br>";
					$text.= "USER:&nbsp;".$this->user."<br><br>";
				}
				break;

			case 1046:
				if(!$this->debug){
					$text = "Name der Datenbank falsch oder Datenbank nicht gefunden!<br>";
					$text.=	"Sie haben eventuell keine Berrechtigung f&uuml;r diese Datenbank!<br><br>";
				}else{
					$text = "Name der Datenbank falsch oder Datenbank nicht gefunden!<br>";
					$text.= "SQL-Fehlernummer:&nbsp;".$e."<br>";
					$text.= "HOST:&nbsp;".$this->host."<br>";
					$text.= "PORT:&nbsp;".$this->port."<br>";
					$text.= "DBname:&nbsp;".$this->db__."<br><br>";
				}
				break;

			case 1064:
				if(!$this->debug){
					$text = "Abfrage nicht ausf&uuml;hrbar!<br>";
					$text.=	"Sie haben eventuell keine Berrechtigung f&uuml;r diese Datenbank!<br><br>";
				}else{
					$text = "Allgemeiner Fehler im SQL-Syntax!<br>";
					$text.= "SQL-Fehlernummer:&nbsp;".$e."<br>";
					$text.= "HOST:&nbsp;".$this->host."<br>";
					$text.= "PORT:&nbsp;".$this->port."<br>";
					$text.= "DBname:&nbsp;".$this->db__."<br>";
					$text.= "Abfrage:&nbsp;<br>".$statement."<br><br>";
					$text.= "SQL_ERROR:&nbsp;<br>".$sql_text;
				}
				break;

			case 1054:
				if(!$this->debug){
					$text = "Abfrage nicht ausf&uuml;hrbar!<br>";
					$text.=	"Sie haben eventuell keine Berrechtigung f&uuml;r diese Datenbank!<br><br>";
				}else{
					$text = "Spalte nicht vorhanden!<br>";
					$text.= "SQL-Fehlernummer:&nbsp;".$e."<br>";
					$text.= "HOST:&nbsp;".$this->host."<br>";
					$text.= "PORT:&nbsp;".$this->port."<br>";
					$text.= "DBname:&nbsp;".$this->db__."<br>";
					$text.= "Abfrage:&nbsp;<br>".$statement."<br><br>";
					$text.= "SQL_ERROR:&nbsp;<br>".$sql_text."<br><br>";
				}
				break;

			case 1146:
				if(!$this->debug){
					$text = "Abfrage nicht ausf&uuml;hrbar!<br>";
					$text.=	"Sie haben eventuell keine Berrechtigung f&uuml;r diese Datenbank!<br><br>";
				}else{
					$text = "Tabelle existiert nicht<br>";
					$text.= "SQL-Fehlernummer:&nbsp;".$e."<br>";
					$text.= "HOST:&nbsp;".$this->host."<br>";
					$text.= "PORT:&nbsp;".$this->port."<br>";
					$text.= "DBname:&nbsp;".$this->db__."<br>";
					$text.= "Abfrage:&nbsp;<br>".$statement."<br><br>";
				}
				break;

			case $e>1046:
				$text = $e."&nbsp;".$sql_text;
				break;

			case $e<1045:
				$text = $e."&nbsp;".$sql_text;
				break;

			case $e>1146:
				$text = $e."&nbsp;".$sql_text;
				break;

		}
		return $text;
	}

	//new Functions
	function mysql_insert_assoc($tablename, $dataarray){
		global $db_link;

		$columns = array_keys($dataarray);
		$values = array_values($dataarray);
	  	$values_number = count($values);

	   	for($i=0;$i<$values_number;$i++){
	   		$value = $values[$i];
			if(get_magic_quotes_gpc()) { $value = stripslashes($value); }
			if(!is_numeric($value))    { $value = "'" . mysql_real_escape_string($value, $db_link) . "'"; }
	     	$values[$i] = $value;
		}

		$sql = "INSERT INTO $tablename ";
		$sql .= "(" . implode(", ", $columns) . ")";
		$sql .= " values ";
		$sql .= "(" . implode(", ", $values) . ")";

		$db->query_db($sql);

		return ($this->result) ? true : false;
	}


	function show_in_table($arr,$styles="",$class=""){
		$count = count($arr);
		$output = "";

		if($count>0){
			reset($arr);
			$num = count(current($arr));

			$wid_str = "";
			
			if(is_array($styles))
			{
            	$wid_str = (array_key_exists('width', $styles)) ? $styles['width'] : '';
			}

			$output.="<table class=\"".$class."\" align=\"".$styles['align']."\" border=\"".$styles['border']."\" cellpadding=\"".$styles['cellpadding']."\" cellspacing=\"".$styles['cellspacing']."\" width=\"". $wid_str."\">\n";
			$output.="<tr>\n";

			foreach(current($arr) as $key => $value){
				$output.="<th>";
				$output.=$key."&nbsp;";
				$output.="</th>\n";
        	}

			$output.="</tr>\n";

			while ($curr_row = current($arr)){
				$output.="<tr>\n";
				$col = 1;

				while (false !== ($curr_field = current($curr_row))){
					$output.="<td>";
					$output.=$curr_field."&nbsp;";
					$output.="</td>\n";
					next($curr_row);
					$col++;
				}
				while($col <= $num){
					$output.="<td>&nbsp;</td>\n";
					$col++;
				}
				$output.="</tr>\n";
				next($arr);
			}
			 $output.="</table>\n";
		}

		return $output;
	}


	function mysql_fetch_fields($table){
        $result = mysql_query("SELECT * FROM $table LIMIT 1");
        $describe = mysql_query("SHOW COLUMNS FROM $table");
        $num = mysql_num_fields($result);
        $output = array();

        for($i = 0;$i<$num;++$i){
                $field = mysql_fetch_field($result, $i);

                $field->auto_increment = (strpos(mysql_result($describe, $i, 'Extra'), 'auto_increment') === FALSE ? 0 : 1);

                $field->definition = mysql_result($describe, $i, 'Type');
                if($field->not_null && !$field->primary_key) $field->definition .= ' NOT NULL';
                if($field->def) $field->definition .= " DEFAULT '" . mysql_real_escape_string($field->def) . "'";
                if($field->auto_increment) $field->definition .= ' AUTO_INCREMENT';
                if($key = mysql_result($describe, $i, 'Key')){
                        if($field->primary_key) $field->definition .= ' PRIMARY KEY';
                        else $field->definition .= ' UNIQUE KEY';
                }
                $field->len = mysql_field_len($result,$i);
                $output[$field->name] = $field;
        }
		return $output;
	}


}

?>